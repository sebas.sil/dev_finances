# Dev Finances

Esta aplicação front-end tem como objetivo auiliar o registro de gastos de um freelancer através de um registro simples de entradas e saídas. Essas informações são armazenadas no storage do próprio navegador.

Esta aplicação faz parte do "Maratona Discover" da Rocketseat como modelo para treino e aprendizagem de desenvolvimento web (HTML e CSS)

## layout

O layout foi entregue pronto e pode ser visto no [Figma](https://www.figma.com/file/TvWLGinPJwydXkZONrBa6b/dev.finance)

## registro das aulas

- [Aula 1](https://www.notion.so/Aula-01-45744a4c8a0c455eaa70fcb3e0c9f79c)
- [Aula 2](https://www.notion.so/Aula-02-e96de013548b42b58f5bee3c803c3a5c)

## imagens

![Print da tela](.gitimages/dev.finances.png)