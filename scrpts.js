Modal = {
    modal: document.querySelector('#modal'),
    open: () => {
        modal.classList.add('active')
    },
    close: () => {
        modal.classList.remove('active')
    }
}